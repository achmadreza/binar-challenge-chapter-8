// import React from "react";

import { Row, Col, Button, Form } from "react-bootstrap";

import React, { Component } from 'react'

export default class FormPlayer extends Component {
  render() {
    return (
        <>
        <div className="form mt-5">
          <Row>
            <Col>
              <h3>Add Player</h3>
              <hr />
            </Col>
            <Row>
              <Col>
                <Form onSubmit={this.props.handleSubmit}>
                  <Form.Group  className="mb-3 ">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                      type="text"
                      name="username"
                      placeholder="Enter username"
                      value={this.props.username}
                      onChange={(e) => this.props.handleChange(e)}
                    />
                  </Form.Group>
    
                  <Form.Group className="mb-3">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type="email"
                      name="email"
                      placeholder="Email Your Address" required
                      value={this.props.email}
                      onChange={(e) => this.props.handleChange(e)}
                    />
                  </Form.Group>
    
                  <Form.Group className="mb-3 ">
                    <Form.Label>Experience</Form.Label>
                    <Form.Control
                      type="number"
                      name="experience"
                      placeholder="Your Experience" required
                      value={this.props.experience}
                      onChange={(e) =>this.props.handleChange(e)}
                    />
                  </Form.Group>
    
                  <Form.Group className="mb-3 ">
                    <Form.Label>Level</Form.Label>
                    <Form.Control
                      type="number"
                      name="level"
                      placeholder="Level" required
                      value={this.props.level}
                      onChange={(e) => this.props.handleChange(e)}
                    />
                  </Form.Group>
    
                  <Button className="btnForm" variant="primary" type="submit">
                    Submit
                  </Button>
                  </Form>
              </Col>
            </Row>
          </Row>
        </div>
        </>
    )
  }
}


// export const FormPlayer = ({username,email,experience,level,handleChange,handleSubmit}) => {
    
  
//     return (
//       <>
//     <div className="mt-5">
//       <Row>
//         <Col>
//           <h3>Add Player</h3>
//           <hr />
//         </Col>
//         <Row>
//           <Col>
//             <Form onSubmit={handleSubmit}>
//               <Form.Group  className="mb-3">
//                 <Form.Label>Username</Form.Label>
//                 <Form.Control
//                   type="text"
//                   name="username"
//                   placeholder="Enter username"
//                   value={username}
//                   onChange={(e) => handleChange(e)}
//                 />
//               </Form.Group>

//               <Form.Group className="mb-3">
//                 <Form.Label>Email</Form.Label>
//                 <Form.Control
//                   type="email"
//                   name="email"
//                   placeholder="Email Your Address"
//                   value={email}
//                   onChange={(e) => handleChange(e)}
//                 />
//               </Form.Group>

//               <Form.Group className="mb-3">
//                 <Form.Label>Experience</Form.Label>
//                 <Form.Control
//                   type="number"
//                   name="experience"
//                   placeholder="Your Experience"
//                   value={experience}
//                   onChange={(e) =>handleChange(e)}
//                 />
//               </Form.Group>

//               <Form.Group className="mb-3">
//                 <Form.Label>Level</Form.Label>
//                 <Form.Control
//                   type="number"
//                   name="level"
//                   placeholder="Level"
//                   value={level}
//                   onChange={(e) => handleChange(e)}
//                 />
//               </Form.Group>

//               <Button variant="primary" type="submit">
//                 Submit
//               </Button>
//             </Form>
//           </Col>
//         </Row>
//       </Row>
//     </div>
//     </>
//   );
// };
