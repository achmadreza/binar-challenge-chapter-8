import React, { Component } from "react";
import FormPlayer from "./FormPlayer";
import NavbarComponent from "./NavbarComponent";
import Search from "./Search";
import {TablePlayer}  from "./TablePlayer";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      players: [],
      username: "",
      email: "",
      experience: "",
      level: 0,
      id: "",
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (this.state.id === "") {
      this.setState({
        players: [
          ...this.state.players,
          {
            id: this.state.players.length + 1,
            username: this.state.username,
            email: this.state.email,
            experience: this.state.experience,
            level: this.state.level,
          },
        ],
      });
    } else {
      const editNotPlayer = this.state.players
        .filter((player) => player.id !== this.state.id)
        .map((filterPlayer) => {
          return filterPlayer;
        });
      this.setState({
        players: [
          ...editNotPlayer,
          {
            id: this.state.players.length + 1,
            username: this.state.username,
            email: this.state.email,
            experience: this.state.experience,
            level: this.state.level,
          },
        ],
      });
    }

    this.setState({
      username: "",
      email: "",
      experience: "",
      level: 0,
      id: "",
    });
  };

  editData = (id) => {
    const editPlayer = this.state.players
      .filter((player) => player.id === id)
      .map((filterPlayer) => {
        return filterPlayer;
      });

    this.setState({
      username: editPlayer[0].username,
      email: editPlayer[0].email,
      experience: editPlayer[0].experience,
      level: editPlayer[0].level,
      id: editPlayer[0].id,
    });
  };

  deleteData = (id) => {
    const deletePlayer = this.state.players
      .filter((player) => player.id !== id)
      .map((filterPlayer) => {
        return filterPlayer;
      });

      this.setState({
        players: deletePlayer
      })
  };

  render() {
    return (
      <>
        <div>
          <NavbarComponent />
          <div className="container mt-4">
            <Search />
            <TablePlayer
              players={this.state.players}
              editData={this.editData}
              deleteData={this.deleteData}
              searchData={this.searchData}
            />
            <FormPlayer
              {...this.state}
              handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
            />
          </div>
        </div>
      </>
    );
  }
}
