import React, { Component } from 'react'

export default class Search extends Component {
  render() {
    return (
      <div>
          <input type="text" name="search" placeholder='Search' className='inpt-lg'/>
          <button className="btn btn-success">Search</button>
      </div>
    )
  }
}
