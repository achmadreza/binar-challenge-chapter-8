import React from "react";
import { Table} from "react-bootstrap";
import './Style.css';




export const TablePlayer = ({ players, editData, deleteData,searchData }) => {
  return (
<>    

    <Table striped bordered hover className="tbl">
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Email</th>
          <th>Experience</th>
          <th>Level</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {players.map((player,index) => {
          return (
            <tr key={index}>
              <td >{index+1}</td>
              <td>{player.username}</td>
              <td>{player.email}</td>
              <td>{player.experience}</td>
              <td>{player.level}</td>
              <td>
                  <button className="btn btn-warning mx-3" onClick={()=> editData(player.id)}>Edit</button>
                  <button className="btn btn-danger" onClick={()=> deleteData(player.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
    </>

  );
};
